use persy_trees::{Db, Tree};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let db = Db::open("./db.persy")?;

    let tree: Tree<String> = db.open_tree("u8-tree")?;

    let bytes = vec![5];

    println!("Inserting");
    let mut tx = tree.transaction()?;
    tx.insert("value".into(), &bytes)?;
    tx.commit()?;

    let mut first_complete = false;

    println!("Two transactions");
    tree.retried_transaction(|mut tx2| {
        println!("Second Value");
        let value2 = tx2.get(&"value".into())?.expect("Value exists");

        println!("Second Insert");
        tx2.insert("value".into(), &[value2[0] - 1])?;

        if !first_complete {
            let mut tx1 = tree.transaction()?;

            println!("First Value");
            let value1 = tx1.get(&"value".into())?.expect("Value exists");

            println!("First Insert");
            tx1.insert("value".into(), &[value1[0] + 1])?;

            println!("First Commit");
            tx1.commit()?;

            first_complete = true;
        }

        println!("Second Commit");
        tx2.commit()?;

        Ok(())
    })?;

    println!("Final get");
    let mut tx = tree.transaction()?;
    let value = tx.get(&"value".into())?.expect("Value exists");

    println!("value: {}", value[0]);

    Ok(())
}
