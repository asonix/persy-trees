use persy_trees::{Db, Tree};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let db = Db::open("./db.persy")?;

    let tree: Tree<String> = db.open_tree("some-tree-name")?;

    let mut tx1 = tree.transaction()?;
    let mut tx2 = tree.transaction()?;

    let put = vec![0, 1, 2, 3, 4];

    tx1.insert("howdy".into(), &put)?;
    tx2.insert("meowdy".into(), &put)?;

    let got1 = tx1.get(&"howdy".into())?.expect("We just inserted this");
    let got2 = tx2.get(&"meowdy".into())?.expect("We just inserted this");

    assert_eq!(got1, put);
    assert_eq!(got2, put);

    for (k, v) in tx1.range(..)? {
        let v = v?;
        println!("{}: {:?}", k, v);
    }
    for (k, v) in tx2.range(..)? {
        let v = v?;
        println!("{}: {:?}", k, v);
    }

    tx1.commit()?;
    tx2.commit()?;

    Ok(())
}
