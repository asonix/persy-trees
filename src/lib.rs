use persy::{
    Config, IndexId, IndexType, Persy, PersyError, PersyId, SegmentId, TransactionConfig,
    TxIndexIter, TxStrategy, ValueMode, PE,
};
use std::borrow::Cow;
use std::marker::PhantomData;
use std::ops::RangeBounds;
use std::path::Path;

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
}

#[derive(Debug)]
pub enum ErrorKind {
    Persy(PersyError),
}

#[derive(Clone)]
pub struct Db {
    persy: Persy,
}

pub struct Tree<K> {
    name: String,
    segment_id: SegmentId,
    _index_id: IndexId,

    persy: Persy,

    _key: PhantomData<K>,
}

pub struct Transaction<'a, K> {
    tree: Cow<'a, Tree<K>>,
    tx: persy::Transaction,
}

pub struct Iter<'a, K: IndexType> {
    iter: TxIndexIter<'a, K, PersyId>,
    segment_id: &'a SegmentId,
}

impl Error {
    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }
}

impl ErrorKind {
    fn is_conflict(&self) -> bool {
        matches!(self, Self::Persy(PersyError::VersionNotLastest))
    }
}

impl Db {
    pub fn open(path: impl AsRef<Path>) -> Result<Self, Error> {
        let persy = Persy::open_or_create_with(path, Config::new(), |_| Ok(()))?;
        Ok(Self { persy })
    }

    pub fn open_tree<K, N>(&self, tree_name: N) -> Result<Tree<K>, Error>
    where
        K: IndexType,
        N: Into<String>,
    {
        let segment_id;
        let index_id;

        let name = tree_name.into();

        let mut tx = self.persy.begin()?;

        if tx.exists_segment(&name)? {
            segment_id = tx.solve_segment_id(&name)?;
            index_id = tx.solve_index_id(&name)?;
        } else {
            segment_id = tx.create_segment(&name)?;
            tx.create_index::<K, PersyId>(&name, ValueMode::Replace)?;
            index_id = tx.solve_index_id(&name)?;
        }
        let tx = tx.prepare()?;
        tx.commit()?;

        Ok(Tree {
            name,
            segment_id,
            _index_id: index_id,

            persy: self.persy.clone(),

            _key: PhantomData,
        })
    }
}

impl<K> Tree<K> {
    pub fn transaction(&self) -> Result<Transaction<'_, K>, Error> {
        let tx = self
            .persy
            .begin_with(TransactionConfig::new().set_strategy(TxStrategy::VersionOnRead))?;

        Ok(Transaction {
            tree: Cow::Borrowed(self),
            tx,
        })
    }

    pub fn retried_transaction<F: FnMut(Transaction<'_, K>) -> Result<T, Error>, T>(
        &self,
        mut callback: F,
    ) -> Result<T, Error>
    where
        K: IndexType,
    {
        loop {
            let tx = self.transaction()?;

            return match (callback)(tx) {
                Ok(t) => Ok(t),
                Err(e) if e.kind().is_conflict() => continue,
                Err(e) => Err(e),
            };
        }
    }
}

impl<'a, K> Transaction<'a, K>
where
    K: IndexType,
{
    pub fn get(&mut self, key: &K) -> Result<Option<Vec<u8>>, Error> {
        if let Some(id) = self.tx.one(&self.tree.name, key)? {
            Ok(self.tx.read(&self.tree.segment_id, &id)?)
        } else {
            Ok(None)
        }
    }

    pub fn range<R: RangeBounds<K>>(&mut self, range: R) -> Result<Iter<'_, K>, Error> {
        Ok(Iter::new(
            self.tx.range(&self.tree.name, range)?,
            &self.tree.segment_id,
        ))
    }

    pub fn insert(&mut self, key: K, value: &[u8]) -> Result<(), Error> {
        if let Some(id) = self.tx.one(&self.tree.name, &key)? {
            self.tx.update(&self.tree.segment_id, &id, value)?;
        } else {
            let id = self.tx.insert(&self.tree.segment_id, value)?;
            self.tx.put(&self.tree.name, key, id)?;
        }
        Ok(())
    }

    pub fn remove(&mut self, key: K) -> Result<(), Error> {
        if let Some(id) = self.tx.one(&self.tree.name, &key)? {
            self.tx.delete(&self.tree.segment_id, &id)?;
            self.tx
                .remove(&self.tree.name, key, None as Option<PersyId>)?;
        }
        Ok(())
    }

    pub fn commit(self) -> Result<(), Error> {
        self.tx.prepare()?.commit()?;
        Ok(())
    }

    pub fn rollback(self) -> Result<(), Error> {
        self.tx.rollback()?;
        Ok(())
    }

    pub fn into_owned(self) -> Transaction<'static, K> {
        Transaction {
            tree: Cow::Owned(self.tree.into_owned()),
            tx: self.tx,
        }
    }
}

impl<'a, K> Iter<'a, K>
where
    K: IndexType,
{
    fn new(iter: TxIndexIter<'a, K, PersyId>, segment_id: &'a SegmentId) -> Self {
        Self { iter, segment_id }
    }
}

impl<'a, K> Iterator for Iter<'a, K>
where
    K: IndexType,
{
    type Item = (K, Result<Vec<u8>, Error>);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let (key, mut values) = self.iter.next()?;

            if let Some(id) = values.next() {
                return match self.iter.tx().read(self.segment_id, &id) {
                    Ok(Some(value)) => Some((key, Ok(value))),
                    Ok(None) => continue,
                    Err(e) => Some((key, Err(e.into()))),
                };
            }
        }
    }
}

impl<'a, K> DoubleEndedIterator for Iter<'a, K>
where
    K: IndexType,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        loop {
            let (key, mut values) = self.iter.next_back()?;

            if let Some(id) = values.next() {
                return match self.iter.tx().read(self.segment_id, &id) {
                    Ok(Some(value)) => Some((key, Ok(value))),
                    Ok(None) => continue,
                    Err(e) => Some((key, Err(e.into()))),
                };
            }
        }
    }
}

impl<K> Clone for Tree<K> {
    fn clone(&self) -> Self {
        Self {
            name: self.name.clone(),
            segment_id: self.segment_id,
            _index_id: self._index_id.clone(),

            persy: self.persy.clone(),

            _key: PhantomData,
        }
    }
}

impl<T: Into<PersyError>> From<PE<T>> for ErrorKind {
    fn from(e: PE<T>) -> Self {
        ErrorKind::Persy(e.persy_error())
    }
}

impl<T> From<T> for Error
where
    ErrorKind: From<T>,
{
    fn from(e: T) -> Self {
        Error {
            kind: ErrorKind::from(e),
        }
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.kind, f)
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        self.kind.source()
    }
}

impl std::fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Persy(_) => write!(f, "Error in storage engine"),
        }
    }
}

impl std::error::Error for ErrorKind {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::Persy(ref err) => Some(err),
        }
    }
}
